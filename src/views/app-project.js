import { html, css } from "lit";
import { Base } from "../Base";

export class AppProject extends Base {

    constructor() {
        super();
        this.project = {};
        this.loaded = false;
        this.list = [
            "Slide 1",
        ];
        // this part is for the slides with reveal.js
        /*Reveal.initialize({
            hash: true,
            plugins: [ RevealMarkdown, RevealHighlight, RevealNotes ]
        });*/
    }

    static get properties() {
        return {
            project: {type: Object},
            loaded: {type: Boolean, state: true},
            list:{type:Array}
        };
    }

    addSlide() {
        const slideNumber = this.list.length + 1;
        this.list.push(`Slide ${slideNumber}`);
        // ecraser la valeur de la liste par une nouvelle
        this.list = [...this.list];
    }


    selectImage(i) {
        const img = document.createElement('img');
        img.src = "https://www.nasa.gov/sites/default/files/thumbnails/image/web_first_images_release.png";
        img.style.width = "auto";
        img.style.height = "600px";
        img.id = i;
        const elem = document.getElementById('slide');
        elem.innerHTML = '';
        elem.appendChild(img);

    }

    // @click=${() => this.cancelClick_cb(record)}


    render() {
        return html`
            <link rel="stylesheet" href="/node_modules/reveal.js/dist/reveal.css">
            <link rel="stylesheet" href="/node_modules/reveal.js/dist/reveal.css">

            <link rel="stylesheet" href="../../dist/reset.css">
            <link rel="stylesheet" href="../../dist/reveal.css">
            <link rel="stylesheet" href="../../dist/theme/black.css">
            <div>
                <a href="/">Retour à la liste des projets</a>
                <div style="border: 1px solid black; margin-top: 20px">
                    <h1>Titre : ${this.project.title}</h1>
                    <p>Description : ${this.project.description}</p>
                    <p>Crée par : ${this.project.owner}</p>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div id="slidesBar" class="col-4">
                        <h1>😎 Slides 😎</h1>
                        ${this.list.map((e, index) => {
                            return html`<h1>${e}</h1>
                            <img id="${index}" @click="${() => this.selectImage(index)}" src="https://www.nasa.gov/sites/default/files/thumbnails/image/web_first_images_release.png" 
                                 class="rounded float-left"
                                 style="width: auto; height: 195px;">`
                        })}
                        <button @click="${this.addSlide}">Add slide</button>
                    </div>
                    <div class="col-8">
                        <h1> Presentation  </h1>
                        <div id="slide">
                        </div>
                    </div>
                </div>
            </div>
            <!--<div class="reveal">
                <div class="slides">
                    <h1>Slide 1</h1>
                    <h1>Slide 2</h1>
                    <h1>Slide 3</h1>
                </div>
            </div>-->
        `;
    }
}

customElements.define("app-project", AppProject);
