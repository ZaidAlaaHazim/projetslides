import {LitElement, html, css} from "lit";
import page from "page";
import {Base} from "../Base";
import {createProjectObject} from "../idbHelper";

export class AppCreateProject extends Base {
    handleCreateProject(e) {
        e.preventDefault();
        const title = document.getElementById("title").value;
        const description = document.getElementById("description").value;

        const newProject = {
            "id": null,
            "title": title,
            "description": description,
            "owner": "",
        }
        createProjectObject(newProject).then(project => {
            page.redirect(`/project/${project.id}`);
        });

    }

    render() {
        return html`
            <div class="mx-auto w-75 mt-5">
                <form @submit="${this.handleCreateProject}" style="margin-top: 50px">
                    <label for="title">Titre de votre projet</label>
                    <input class="form-control" type="text" id="title"/>
                    <label for="description" class="mt-3">Description</label>
                    <input class="form-control" type="text" id="description"/>
                    <button class="btn text-white mt-4" style="background-color: #124470">Créer le projet</button>
                </form>
            </div>
        `;
    }
}

customElements.define("app-create-project", AppCreateProject);
