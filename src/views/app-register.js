import {LitElement, html, css} from "lit";
import page from "page";
import {Base} from "../Base";
import {createUser} from "../firebase";
import googleSignIn from "../login/auth_google_signin_popup";
import facebookSignIn from "../login/auth_facebook_signin_popup";

export class AppRegister extends Base {
    async handleRegister(e) {
        e.preventDefault();
        const email = e.target.querySelector("input[type=email]").value;
        const pwd = e.target.querySelector("input[type=password]").value;

        const user = await createUser(email, pwd);
        if (user) {
            page("/");
        }
    }

    async handleGoogleRegister(e) {
        e.preventDefault();

        await googleSignIn();
    }

    async handleFacebookRegister(e) {
        e.preventDefault();

        await facebookSignIn();
    }

    render() {
        return html`
            <div class="mx-auto text-center w-75 mt-5">
                <form @submit="${this.handleRegister}">
                    <div class="text-start ">
                        <label for="registerMail">Adresse mail</label>
                    </div>
                    <input class="form-control" type="email" id="registerMail" />

                    <div class="text-start">
                        <label class="mt-4" for="registerPwd">Mot de passe</label>
                    </div>
                    <input class="form-control" type="password" id="registerPwd" />
                    <div class="text-start mt-3">
                        <button class="btn text-white" style="background-color: #124470">Créer mon compte</button>
                    </div>
                </form>
                <hr>
                <form @submit="${this.handleGoogleRegister}" style="display: inline">
                    <button class="btn btn-warning">Créer avec Google</button>
                </form>
                <form @submit="${this.handleFacebookRegister}" style="display: inline">
                    <button class="btn btn-primary">Créer avec Facebook</button>
                </form>
            </div>
        `;
    }
}

customElements.define("app-register", AppRegister);