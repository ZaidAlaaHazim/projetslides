import {LitElement, html, css} from "lit";
import page from "page";
import {Base} from "../Base";
import {loginUser} from "../firebase";
import googleSignIn from "../login/auth_google_signin_popup";
import facebookSignIn from "../login/auth_facebook_signin_popup";

export class AppLogin extends Base {
    async handleLogin(e) {
        e.preventDefault();
        const email = e.target.querySelector("input[type=email]").value;
        const pwd = e.target.querySelector("input[type=password]").value;

        const user = await loginUser(email, pwd);
        if (user) {
            page.redirect("/");
        }
    }

    async handleGoogleLogin(e) {
        e.preventDefault();

        await googleSignIn();
    }

    async handleFacebookLogin(e) {
        e.preventDefault();

        await facebookSignIn();
    }

    render() {
        return html`
            <div class="mx-auto w-75 mt-5">
                <form @submit="${this.handleLogin}">
                    <label for="loginMail">Adresse mail</label>
                    <input class="form-control" type="email" id="loginMail"/>

                    <label class="mt-4" for="loginPwd">Mot de passe</label>
                    <input class="form-control" type="password" id="loginPwd"/>

                    <div class="mt-3 text-center">
                        <button class="btn text-white" style="background-color: #124470">Se connecter</button>
                    </div>
                </form>
                <hr>
                <div class="text-center">
                    <button class="btn btn-warning" @click="${this.handleGoogleLogin}">Se connecter avec Google</button>
                    <button class="btn btn-primary" @click="${this.handleFacebookLogin}">Se connecter avec Facebook
                    </button>
                </div>
            </div>
        `;
    }
}

customElements.define("app-login", AppLogin);