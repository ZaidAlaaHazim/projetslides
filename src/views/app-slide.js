import {LitElement, html, css} from "lit";
import {Base} from "../Base";
import Reveal from 'reveal.js';
import Markdown from 'reveal.js/plugin/markdown/markdown.esm.js';

// let deck = new Reveal({
//     plugins: [ Markdown ]
// })



export class AppSlide extends Base {
    constructor() {
        super();
    }

    render() {
        return html`
        <h1>Slide page</h1>
        `;
    }
}

// deck.initialize();


customElements.define("app-slide", AppSlide);
