import {LitElement, html, css} from 'lit';
import {Base} from '../Base';
// import Project from "../components/Project";
import "../components/ProjectList";

export class AppHome extends Base {
    constructor() {
        super();

        this.items = [];
    }

    static get properties() {
        return {
            items: {type: Array},
        };
    }

    render() {
        return (
            html`
                <project-list .items="${this.items}"/>
            `
        );
    }
}

customElements.define('app-home', AppHome);