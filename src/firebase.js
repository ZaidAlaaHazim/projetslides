import { initializeApp } from "firebase/app";
import {
    getDatabase,
    set,
    ref,
    push,
    serverTimestamp,
    onChildAdded,
    onChildRemoved,
    onValue,
} from "firebase/database";
import {
    getAuth,
    signInWithEmailAndPassword,
    createUserWithEmailAndPassword,
    setPersistence,
    browserLocalPersistence,
    onAuthStateChanged,
} from "firebase/auth";

import firebaseConfig from "./firebase.json";

const app = initializeApp(firebaseConfig);
const database = getDatabase(app);
const auth = getAuth(app);

export function getUser() {
    return auth.currentUser;
}

export function getAuthState(cb = () => {}) {
    onAuthStateChanged(auth, (user) => {
        if (user) return cb(user);
        cb(false);
    });
}

export async function createUser(email, password) {
    return setPersistence(auth, browserLocalPersistence).then(() => {
        return createUserWithEmailAndPassword(auth, email, password);
    });
}

export async function loginUser(email, password) {
    return setPersistence(auth, browserLocalPersistence).then(() => {
        return signInWithEmailAndPassword(auth, email, password);
    });
}

export async function signOutUser() {
    auth.signOut().then(function () {
        console.log("User logged out")
    }, function (error) {
        console.log("Error signing out : ", error);
    });
}