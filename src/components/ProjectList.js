import {Base} from "../Base";
import {LitElement, html, css} from "lit";
import {deleteFromProjects} from "../idbHelper";
import page from "page";
import "../components/Project";

export class ProjectList extends Base {
    constructor() {
        super();

        this.items = [];
    }

    static get properties() {
        return {
            items: {type: Array},
        };
    }

    render() {
        if (this.items.length === 0) {
            return (
                html`
                    <div class="text-center mt-5">
                        <h2>Vous n'avez pas encore créé de projets.</h2>
                        <a class="btn text-white" style="background-color: #124470" href="/project/new">Créer mon premier project</a>
                    </div>
                `
            );
        }
        else {
            return (
                this.items.map(project => html`
                <project-div .id="${project.id}" .title="${project.title}" .description="${project.description}" .owner="${project.owner}" />
            `)
            );
        }
    }
}

customElements.define("project-list", ProjectList);
