import {Base} from "../Base";
import {LitElement, html, css} from "lit";
import {deleteFromProjects} from "../idbHelper";
import page from "page";


export class Project extends Base {
    constructor() {
        super();

        this.id = null;
        this.title = "";
        this.description = "";
        this.owner = "";
        this.loaded = false;
    }

    static get properties() {
        return {
            id: {type: Number},
            title: {type: String},
            description: {type: String},
            owner: {type: String},
            loaded: {type: Boolean, state: true},
        };
    }

    goToPage() {
        page.redirect(`/project/${this.id}`);
    }

    async remove(id) {
        await deleteFromProjects(id);
    }

    render() {
        return html`
            <div class="mx-auto" style="margin-top: 50px">
                <div class="ps-5 pt-3 pb-4 pe-5 w-75 mx-auto border-0 rounded-3" style="background-color: #F0F8FF">
                    <h5 class="float-end">#${this.id}</h5>
                    <h2>${this.title}</h2>
                    <small>${this.description}</small>
                    <div class="mt-5">
                        <button class="btn text-white" style="background-color: #124470" @click="${this.goToPage}">Ouvrir</button>
                        <button class="btn btn-outline-dark" @click="${() => this.remove(this.id)}">Supprimer</button>    
                    </div>
                </div>
            </div>
        `;
    }
}

customElements.define("project-div", Project);
