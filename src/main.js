import page from "page";
import checkConnectivity from "network-latency";
import {
    setRessources,
    setRessource,
    getRessources,
    getRessource, setProjects,
} from "./idbHelper";

import {
    getAuth,
} from "firebase/auth";

import {getProjects, getProject} from "./api/projects";
import "./views/app-home";
import {getAuthState, getUser, signOutUser} from "./firebase";
import {initializeApp} from "firebase/app";
import firebaseConfig from "./firebase.json";
import {AppSlide} from "./views/app-slide";
import {AppLogin} from "./views/app-login";

export function closeLogin() {
    debugger;
    AppLogin.active = false;
}


(async (root) => {
    const main = root.querySelector("main");
    const header = root.querySelector("header");

    const app = initializeApp(firebaseConfig);
    const auth = getAuth(app);

    checkConnectivity({
        timeToCount: 3,
        threshold: 2000,
        interval: 3000,
    });

    let NETWORK_STATE = true;

    document.addEventListener("connection-changed", ({detail: state}) => {
        NETWORK_STATE = state;

        if (state) {
            document.documentElement.style.setProperty("--app-bg-color", "royalblue");
        } else {
            document.documentElement.style.setProperty("--app-bg-color", "#858994");
        }
    });

    const AppHome = main.querySelector("app-home");
    const AppProject = main.querySelector("app-project");
    const AppLogin = main.querySelector("app-login");
    const AppRegister = main.querySelector("app-register");
    const AppCreateProject = main.querySelector("app-create-project");
    const AppSlide = main.querySelector("app-slide");

    let isUserLogged = getUser();

    getAuthState((user) => {
        isUserLogged = user;

        if (isUserLogged) {
            const queryString = new URLSearchParams(location.search);

            return page(queryString.get("from") || location.pathname);
        }
        // page(`/login?from=${location.pathname}`);
    });

    // clean (() => {
    //     console.log('evertging is clean');
    //
    // });

    page("*", async (ctx, next) => {
        AppLogin.active = false;

        const loginBtn = header.querySelector(".loginLogout");
        const registerBtn = header.querySelector(".signUp");
        const newProject = header.querySelector(".newProject");

        // console.log(getUser());
        if (!getUser()) {
            loginBtn.textContent = "Se connecter";
            loginBtn.href = "/login";
            registerBtn.style.display = "inline";
            newProject.style.display = "none";
        } else {
            loginBtn.textContent = "Se déconnecter";
            loginBtn.href = "/logout";
            registerBtn.style.display = "none";
            newProject.style.display = "inline";
        }

        next();
    });

    page("/", async (ctx) => {
        await import("./views/app-home");


        if (getUser()) {
            const projects = await getProjects();
            let storedProjects = [];
            let itemsToShow = [];

            // Afficher les projets du user connecté
            for (let item of projects.items) {
                if (item.owner === getUser().email) {
                    itemsToShow.push(item);
                }
            }

            if (NETWORK_STATE) {
                // Ici petit problème, si je remets ces 2 lignes, à chaque refresh de la page il y aura un nouveau projet ajouté
                // const storedProjects = await getProjects();
                // storedProjects = await setRessources(projects);

                storedProjects = itemsToShow;
            } else {
                storedProjects = await getRessources();
            }
            AppHome.items = storedProjects;

            AppProject.active = false;
            AppRegister.active = false;
            AppCreateProject.active = false;
            AppSlide.active = false;
            AppLogin.active = false;
            AppHome.active = true;
        }

    });

    page("/register", async () => {
        await
            import ("./views/app-register.js");

        AppLogin.active = false;
        AppCreateProject.active = false;
        AppRegister.active = true;
    });

    page("/login", async () => {
        await
            import ("./views/app-login.js");

        AppLogin.active = true;
        AppHome.active = false;
        AppRegister.active = false;
        AppCreateProject.active = false;
        AppProject.active = false;
    });

    page("/logout", async () => {
        await signOutUser();
        page("/login");

        AppLogin.active = true;
        AppRegister.active = false;
        AppCreateProject.active = false;
        AppProject.active = false;
        AppSlide.active = false;
    });

    /*page("/projects", async () => {
        await
            import ("./views/app-project.js");
        // AppProject.Active = true;

    });*/

    page("/project/new", async () => {
        await
            import ("./views/app-create-project.js");
        AppCreateProject.active = true;
        AppHome.active = false;
        AppProject.active = false;
        AppSlide.active = false;
        AppRegister.active = false;
        AppLogin.active = false;
    });

    page("/project/:id", async ({ params }) => {
        await
            import ("./views/app-project.js");

        const projects = await getProjects();
        const project = projects.items.find((p) => p.id === parseInt(params.id));
        let storedProject = {};

        if (NETWORK_STATE) {
            // storedProject = await setRessource(project);
            storedProject = project;
        } else {
            storedProject = await getRessource(params.id);
        }

        AppProject.project = { ...storedProject };

        AppHome.active = false;
        AppCreateProject.active = false;
        AppProject.active = true;
    });

    page("/slide", async () => {
        await
            import ("./views/app-slide.js");
        AppSlide.Active = true;
        AppHome.setAttribute("visibility", "visible");
    });

    page();
})(document.querySelector("#app"));
