import { getAuth, signInWithPopup, FacebookAuthProvider, signInWithRedirect, getRedirectResult } from "firebase/auth";
import page from "page";

const auth = getAuth();
const provider = new FacebookAuthProvider();

const facebookSignIn = () => {
    signInWithPopup(auth, provider)
        .then((result) => {
            // The signed-in user info.
            const user = result.user;

            // This gives you a Facebook Access Token. You can use it to access the Facebook API.
            const credential = FacebookAuthProvider.credentialFromResult(result);
            const accessToken = credential.accessToken;
            page("/");
        })
        .catch((error) => {
            const errorCode = error.code;
            // const errorMessage = error.message;
            const email = error.customData.email;
            const credential = FacebookAuthProvider.credentialFromError(error);
        });
}
export default facebookSignIn;