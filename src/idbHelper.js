import { openDB } from "idb";
import {getProjects, updateProjects} from "./api/projects";
import {getAuthState, getUser} from "./firebase";



const PROJECT_NAME = "Projects";
export function initDB() {
    return openDB("projet-slides", 1, {
        upgrade(db) {
            // Create a store of objects
            const projectStore = db.createObjectStore(PROJECT_NAME, {
                // The 'id' property of the object will be the key.
                keyPath: "id",
                autoIncrement: true
            });
            // Create an index on the 'date' property of the objects.
            projectStore.createIndex("id", "id");
            // projectStore.createIndex("project", "project");
        },
    });
}

let NETWORK_STATE = true;

document.addEventListener("connection-changed", async ({ detail: state }) => {
    NETWORK_STATE = state;
});

export async function setRessources(data) {
    const db = await initDB();
    const tx = db.transaction(PROJECT_NAME, "readwrite");
    // if (data) {
    //     data.items.forEach((item) => {
            tx.store.put(data);
        // });
    // }

    // await tx.done;
    return db.getAllFromIndex(PROJECT_NAME, "id");
}

export async function setRessource(data) {
    const db = await initDB();
    const tx = db.transaction(PROJECT_NAME, "readwrite");
    await tx.store.put(data);
    return db.getFromIndex(PROJECT_NAME, "id", data.id);
}

export async function setProjects(projects) {
    const db = await initDB();
    const tx = db.transaction(PROJECT_NAME, "readwrite");
    await tx.store.put({});
    await tx.store.put(projects);
    return db.get(PROJECT_NAME, "projects");
}

export async function getRessources() {
    const db = await initDB();
    return db.getAllFromIndex(PROJECT_NAME, "id");
}

export async function getRessourcesFromIndex(indexName) {
    const db = await initDB();
    return db.getAllFromIndex(PROJECT_NAME, indexName);
}

export async function getRessource(id) {
    const db = await initDB();
    return db.get(PROJECT_NAME, "projects");
    // return db.getFromIndex(PROJECT_NAME, "id", id);
}

export async function unsetRessource(id) {
    const db = await initDB();
    await db.delete(PROJECT_NAME, id);
}

export async function createProjectObject(project) {
    const db = await initDB();
    const projects = await getProjects();
    if (projects.items.length > 0) {
        project.id = projects.items[projects.items.length - 1].id + 1;
    }
    else {
        project.id = 1;
    }

    project.owner = getUser().email;
    const tx = db.transaction(PROJECT_NAME, "readwrite");
    await tx.store.put(project);
    await addToProjects(project);
    // return db.get(PROJECT_NAME, "projects");
    return project;
}

export async function addToProjects(project) {
    const projects = await getProjects();
    projects.items.push(project);
    let updatedProjects = { ...projects };

    if (NETWORK_STATE) {
        await updateProjects(updatedProjects);
    }

    await setProjects(updatedProjects);
}

function debounce(func, timeout = 300) {
    let timer;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => {
            func.apply(this, args);
        }, timeout);
    };
}
const _updateProjects = debounce(updateProjects);

export async function deleteFromProjects(id) {
    const projects = await getProjects();

    projects.items = projects.items.filter((item) => item.id !== id);
    let updatedProjects = { ...projects };

    if (NETWORK_STATE) {
        updatedProjects = { ...updatedProjects };
        _updateProjects(updatedProjects);
    }

    return await setProjects(updatedProjects);
}
