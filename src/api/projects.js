import { createRequest } from './api';
import {setProjects} from "../idbHelper";

const request = createRequest();

export function getProjects() {
    return request.get("/projects")
        .then(({ data }) => data)
        .catch(console.error);
}

export function getProject(projectId) {
    return request.get(`/projects/${projectId}`)
        .then(({ data }) => data)
        .catch(console.error);
}

export function updateProjects(projects) {
    return request
        .put("/projects", projects)
        .then(({ data }) => data)
        .catch(console.error);
}